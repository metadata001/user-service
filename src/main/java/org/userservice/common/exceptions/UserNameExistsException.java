package org.userservice.common.exceptions;

public class UserNameExistsException extends RuntimeException {
    public UserNameExistsException(String message) {
        super(message, new RuntimeException());
    }
}
