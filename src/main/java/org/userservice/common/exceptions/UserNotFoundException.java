package org.userservice.common.exceptions;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String message) {
        super(message, new RuntimeException());
    }

}
