package org.userservice.common.exceptions;

public class UserNotUpdatedException extends RuntimeException {

    public UserNotUpdatedException(String message) {
        super(message, new RuntimeException());
    }

}
