package org.userservice.common.exceptions.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.userservice.common.exceptions.InvalidUserNameException;
import org.userservice.common.exceptions.UserNameExistsException;
import org.userservice.common.exceptions.UserNotFoundException;
import org.userservice.common.exceptions.UserNotUpdatedException;
import java.util.*;
import java.util.stream.Collectors;

import static org.userservice.common.helpers.ErrorValidationKeys.getTargets;

@RestControllerAdvice
public class ControllerExceptionHandler {
    public static final String ERRORS = "errors";
    public static final String UNDEFINED = "undefined";

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity getArgValidationError(MethodArgumentNotValidException exception) {
        var errors = getArgumentErrors(exception);
        var map = mappedArgumentErrors(errors, getTargets());
        return new ResponseEntity<>(map, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException exception) {
        var error = getError(exception);
        var map = mappedArgumentErrors(error, getTargets());
        return new ResponseEntity<>(map, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNameExistsException.class)
    public final ResponseEntity<Object> handleUserNameExistsException(UserNameExistsException exception) {
        var error = getError(exception);
        var map = mappedArgumentErrors(error, getTargets());
        return new ResponseEntity<>(map, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNotUpdatedException.class)
    public final ResponseEntity<Object> handleUserNotUpdatedException(UserNotUpdatedException exception) {
        var error = getError(exception);
        var map = mappedArgumentErrors(error, getTargets());
        return new ResponseEntity<>(map, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidUserNameException.class)
    public final ResponseEntity<Object> handleInvalidUserNameException(InvalidUserNameException exception) {
        var error = getError(exception);
        var map = mappedArgumentErrors(error, getTargets());
        return new ResponseEntity<>(map, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleExceptions(Exception ex) {
        var error = new ErrorResponseHandler(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        return new ResponseEntity<>(error, error.getStatus());
    }

    /**
     * @param errors represents a list of string validation errors
     * @param errorCodes a map of string error codes; ex. {password : {"password invalid"}}
     * @return Map; ex. {errors: {password: {"password is invalid"}}}
     *<br><br>
     * This method iterates over a map of pre-defined error codes which is compared against a list
     * of error strings. An error string is added to the map if the error code is contained in the string.
     * If there are still error elements in the errors list after iteration, remaining elements are mapped
     * as undefined errors. Map entries are then removed if they are empty after iteration. It is best practice
     * to avoid unmapped errors. Errors should be mapped in the ErrorValidationKeys.class.
     * <br><br>
     * To increase performance, each time an error string is added to the map,it is taken out of the list
     * and the index is decremented for slippage, so there are fewer error strings to iterate over each cycle.
     * <br><br>
     * The completed map is then put into another map with an "error" key for consumption. see @return
     * annotation for return structure.
     */
    private Map mappedArgumentErrors(List<String> errors, Map<String, Set<String>> errorCodes) {
        var errorsMap = new HashMap<>();
        for(Map.Entry errorCode : errorCodes.entrySet()) {
            for(int i = 0; i < errors.size(); i++) {
                if(errors.get(i).contains(errorCode.getKey().toString())) {
                    errorCodes.get(errorCode.getKey().toString()).add(errors.get(i));
                    errors.remove(errors.get(i));
                    --i;
                }
            }
        }
        errorCodes.entrySet().removeIf(entry -> entry.getValue().isEmpty());
        if(!errors.isEmpty()) {
            errorCodes.put(UNDEFINED, new HashSet<>(errors));
        }
        errorsMap.put(ERRORS, errorCodes);
        return errorsMap;
    }

    /**
     * @param exception takes a class that implements BindException interface
     * @return ArrayList - returns an arrayList of string validation errors;
     */
    private ArrayList<String> getArgumentErrors(BindException exception) {
        return exception.getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    /**
     * @param exception takes a class that extends exception
     * @return ArrayList - returns the error message in a list for consumption;
     */
    private ArrayList<String> getError(Exception exception) {
        var list = new ArrayList<String>();
        list.add(exception.getMessage());
        return list;
    }

}