package org.userservice.common.exceptions.handler;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class ErrorResponseHandler {
    private HttpStatus status;
    @JsonProperty("time stamp")
    private String timeStamp;
    private String message;

    public ErrorResponseHandler() {
        this.timeStamp = this.formattedDateTime();
    }

    public ErrorResponseHandler(HttpStatus status, String message) {
        this();
        this.status = status;
        this.message = message;
    }

    public ErrorResponseHandler(HttpStatus status, String timeStamp, String message) {
        this.timeStamp = timeStamp;
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getMessage() {
        return message;
    }

    public ErrorResponseHandler setStatus(HttpStatus status) {
        this.status = status;
        return this;
    }

    public ErrorResponseHandler setMessage(String message) {
        this.message = message;
        return this;
    }

    private String formattedDateTime() {
        SimpleDateFormat formatTime = new SimpleDateFormat("hh:mm aa");
        String time = formatTime.format(new Date());
        var zoneId = ZoneId.systemDefault().toString();
        var timeZone = String.valueOf(zoneId);
        var d = LocalDateTime.now();
        return d.getDayOfMonth() + "-" + d.getMonth() + "-" +
                   d.getYear() + " " + time + " " + timeZone;
    }


}
