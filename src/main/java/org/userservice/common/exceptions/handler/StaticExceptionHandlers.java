package org.userservice.common.exceptions.handler;

import com.mongodb.client.result.DeleteResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.userservice.common.exceptions.InvalidUserNameException;
import org.userservice.common.exceptions.UserNameExistsException;
import org.userservice.common.exceptions.UserNotFoundException;
import org.userservice.user.dto.RemoveUserRequest;
import org.userservice.user.dto.UpdateUserRequest;
import org.userservice.user.model.BaseUser;

public class StaticExceptionHandlers {
    private static final Logger logger = LoggerFactory.getLogger(StaticExceptionHandlers.class);

    public static void checkUserNameValid(String username) throws InvalidUserNameException {
        if(username == null || username.length() == 0) {
            var message = "username is missing for request";
            logger.warn(message);
            throw new InvalidUserNameException(message);
        }
    }

    public static <T extends BaseUser> void checkUserNameAvailable(T user) {
        if(user != null) {
            var message = "username already exists";
            logger.warn(message);
            throw new UserNameExistsException(message);
        }
    }

    public static <T extends BaseUser> void checkUserFound(String username, T result) throws UserNotFoundException {
        if(result == null) {
            var message = String.format("user was not found for request %n %s", username);
            logger.warn(message);
            throw new UserNotFoundException(message);
        }
    }

    public static <T extends BaseUser> void checkUserFound(T user, UpdateUserRequest request) throws UserNotFoundException {
        if(user == null) {
            var message = String.format("user was not updated for request %n %s, %s, %s, %s",
                    request.getUsername(), request.getEmail(), request.getFirstname(), request.getLastname());
            logger.warn(message);
            throw new UserNotFoundException(message);
        }
    }

    public static void checkUserRemoved(DeleteResult result, RemoveUserRequest request) throws UserNotFoundException {
        if(result.getDeletedCount() == 0L) {
            var message = String.format("user was not removed for request %n %s", request.getUsername());
            logger.warn(message);
            throw new UserNotFoundException(message);
        }
    }

}
