package org.userservice.common.helpers;

import io.micrometer.common.util.StringUtils;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.lang.Nullable;

public class NonNullUpdate extends Update {
    /**
     * Static factory method to create an Update using the provided key
     *
     * @param key the field to update.
     * @return new instance of {@link Update}.
     * <br><br>
     * updated method to only accept non-null string values. This was preferable
     * to making multiple if statements to check for null strings when updating
     * multiple elements in a database collection.
     * */
    @Override
    public Update set(String key, @Nullable Object value) {
        if(value instanceof String && StringUtils.isNotBlank(value.toString())) {
            addMultiFieldOperation("$set", key, value);
            return this;
        }
        return null;
    }
}
