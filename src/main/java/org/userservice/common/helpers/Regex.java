package org.userservice.common.helpers;

public class Regex {
    public static final String LOWERCASE = ".*[a-z]+.+";
    public static final String UPPERCASE = ".*[A-Z]+.*";
    public static final String DIGIT = ".*[0-9]+.*";
    public static final String SPECIAL_CHAR = ".*[@#$&!+=-]+.*";
    public static final String EMAIL = ".*[@]+.*[.]+.*";
}
