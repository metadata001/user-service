package org.userservice.common.helpers;

public class UserCollectionKeys {
    public static final String USERNAME = "username";
    public static final String VERSION = "version";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String EMAIL = "email";
}
