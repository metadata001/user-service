package org.userservice.common.model;

import org.springframework.data.annotation.Id;
import javax.persistence.GeneratedValue;

public class BaseEntity {
    @Id
    @GeneratedValue
    private String id;

    public BaseEntity() {}

    public BaseEntity(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public BaseEntity setId(String id) {
        this.id = id;
        return this;
    }

}
