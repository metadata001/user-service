package org.userservice.common.model;

import jakarta.validation.constraints.NotBlank;
import org.springframework.data.mongodb.core.mapping.Field;

public class VersionedEntity extends BaseEntity {
    @Field
    @NotBlank(message = "version is required")
    private int version;

    public VersionedEntity() {
        super();
        this.version = 1;
    }

    public VersionedEntity(String id, int version) {
        super(id);
        this.version = version;
    }

    public int getVersion() {
        return version;
    }

    public VersionedEntity setVersion(int version) {
        this.version = version;
        return this;
    }

}
