package org.userservice.user.controller;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.userservice.user.dto.CreateUserRequest;
import org.userservice.user.dto.RemoveUserRequest;
import org.userservice.user.dto.UpdateUserRequest;
import org.userservice.user.model.RegisteredUser;
import org.userservice.user.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create-registered")
    public ResponseEntity<RegisteredUser> createRegisteredUser(@RequestBody @Valid CreateUserRequest request) {
        var result = userService.createRegisteredUser(request);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/get-registered")
    public ResponseEntity<RegisteredUser> getRegisteredUser(@RequestParam String username) {
        var result = userService.getRegisteredUser(username);
        return ResponseEntity.ok(result);
    }

    @PutMapping("/update-registered")
    public ResponseEntity<UpdateResult> updateRegisteredUser(@RequestBody @Valid UpdateUserRequest request) {
        var result = userService.updateRegisteredUser(request);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/remove-registered")
    public ResponseEntity<DeleteResult> deleteRegisteredUser(@RequestBody @Valid RemoveUserRequest request) {
        var result = userService.removeRegisteredUser(request);
        return ResponseEntity.ok(result);
    }

}
