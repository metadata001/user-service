package org.userservice.user.dto;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import org.userservice.common.helpers.Regex;

import static jakarta.validation.constraints.Pattern.Flag.DOTALL;

public class CreateUserRequest {
        @NotBlank(message = "username is required")
        @Size(min = 6, message = "username must have at least 6 characters")
        private String username;
        @NotBlank(message = "username is required")
        @Pattern(regexp = Regex.EMAIL, flags = DOTALL, message = "email must follow some format e.g. 'bobjoe@domain.com'")
        private String email;
        private String firstname;
        private String lastname;

        public String getUsername() {
                return username;
        }

        public CreateUserRequest setUsername(String username) {
                this.username = username;
                return this;
        }

        public String getEmail() {
                return email;
        }

        public CreateUserRequest setEmail(String email) {
                this.email = email;
                return this;
        }

        public String getFirstname() {
                return firstname;
        }

        public CreateUserRequest setFirstname(String firstname) {
                this.firstname = firstname;
                return this;
        }

        public String getLastname() {
                return lastname;
        }

        public CreateUserRequest setLastname(String lastname) {
                this.lastname = lastname;
                return this;
        }

}
