package org.userservice.user.dto;

import jakarta.validation.constraints.NotBlank;

public class GetUserRequest {
    @NotBlank(message = "username is required")
    private String username;

    public String getUsername() {
        return username;
    }

    public GetUserRequest setUsername(String username) {
        this.username = username;
        return this;
    }
}
