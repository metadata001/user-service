package org.userservice.user.dto;

import jakarta.validation.constraints.NotBlank;

public class RemoveUserRequest {
    @NotBlank(message = "username is required")
    private String username;

    public String getUsername() {
        return username;
    }

    public RemoveUserRequest setUsername(String username) {
        this.username = username;
        return this;
    }
}
