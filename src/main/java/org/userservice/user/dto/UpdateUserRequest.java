package org.userservice.user.dto;

import jakarta.validation.constraints.NotBlank;

public class UpdateUserRequest {
    @NotBlank(message = "username is required")
    private String username;
    private String email;
    private String firstname;
    private String lastname;

    public String getUsername() {
        return username;
    }

    public UpdateUserRequest setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UpdateUserRequest setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public UpdateUserRequest setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public UpdateUserRequest setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

}
