package org.userservice.user.dto;

import java.io.Serializable;

public record UserDTO (
        String id,
        int version,
        String username,
        String email,
        String firstname,
        String lastname)
        implements Serializable {}
