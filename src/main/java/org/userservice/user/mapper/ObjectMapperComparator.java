package org.userservice.user.mapper;

import java.lang.reflect.Field;
import java.util.*;

public class ObjectMapperComparator {

    /**
     * @param original generic object type
     * @param update generic object type
     * <br><br>
     * @return boolean; primitive true or false value
     * <br><br>
     * This method takes two object, extract the fields names and values into a map via reflection,
     * then enters them into a map as key value fields for value comparison. If the update object
     * contains a field that the original does not have then the value is skipped.
     * <br><br>
     * This method is mainly for determining whether a mongo document will be updated based on if
     * there is a change in between the document "original" and request "update" fields. If there are
     * no compatible fields for comparison, this method will return false. If no change is detected
     * between available matching fields then this method will return false. If either value passed
     * into the method is null, this method will also return false.
     */
    public static <T> boolean isUpdated(T original, T update) {
        if(original == null || update == null) {
            return false;
        }

        var original2 = original.getClass().getDeclaredFields();
        var update2 = update.getClass().getDeclaredFields();

        var originalMap = new HashMap<>();
        var updatedMap = new HashMap<>();
        var updatedKeys = new Object[update2.length];
        int index = 0;

        try {
            for (Field field : original2) {
                field.setAccessible(true);
                var name = field.getName();
                var value = field.get(original);
                originalMap.put(name, value);
            }
            for(Field field : update2) {
                field.setAccessible(true);
                var name = field.getName();
                var value = field.get(update);
                updatedMap.put(name, value);
                updatedKeys[index] = name;
                index++;
            }
            var keys = updatedMap.keySet();
            for(int i = 0; i < keys.size(); i++) {
                if(!originalMap.containsKey(updatedKeys[i])) {
                    continue;
                }
                var oVal = originalMap.get(updatedKeys[i]);
                var upVal = updatedMap.get(updatedKeys[i]);

                if(oVal != null && !oVal.equals(upVal)) {
                    return true;
                }
                else if(oVal instanceof Number && oVal != upVal) {
                    return true;
                }
            }
        } catch(Exception e) {
            throw new IllegalArgumentException();
        }
        return false;
    }

}
