package org.userservice.user.mapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.userservice.common.exceptions.UserNotUpdatedException;
import org.userservice.user.dto.UpdateUserRequest;
import org.userservice.user.model.RegisteredUser;

import static org.userservice.user.mapper.ObjectMapperComparator.isUpdated;

public class RequestResultMappings {
    private static final Logger logger = LoggerFactory.getLogger(RequestResultMappings.class);
    
    public static void checkUserRequestWillUpdate(RegisteredUser user, UpdateUserRequest request) {
        if(!isUpdated(user, request)) {
            logger.warn("User was not updated, not update provided");
            throw new UserNotUpdatedException("user was not updated, no update provided");
        }
    }

}
