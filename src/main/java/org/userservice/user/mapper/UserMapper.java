package org.userservice.user.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.userservice.user.dto.CreateUserRequest;
import org.userservice.user.dto.UserDTO;
import org.userservice.user.model.RegisteredUser;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "version", ignore = true)
    RegisteredUser map(CreateUserRequest user);

    @Mapping(target = "version", ignore = true)
    RegisteredUser map(UserDTO userDTO);

    UserDTO map(RegisteredUser user);

}
