package org.userservice.user.model;

import org.springframework.data.mongodb.core.mapping.Field;
import org.userservice.common.model.VersionedEntity;

public class BaseUser extends VersionedEntity {
    @Field(name = "username")
    private String username;

    public BaseUser() {}

    public BaseUser(String username) {
        super();
        this.username = username;
    }

    public BaseUser(String id, int version, String username) {
        super(id, version);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public BaseUser setUsername(String username) {
        this.username = username;
        return this;
    }

}
