package org.userservice.user.model;

final class GuestUser extends BaseUser {
    private static final String USERNAME = "guest";
    private static final String FIRSTNAME = "guest";
    private static final String LASTNAME = "guest";
    private static GuestUser instance = null;

    private GuestUser() {
        super(USERNAME);
    }

    public static synchronized GuestUser getInstance() {
        if(instance == null) {
            instance = new GuestUser();
        }
        return instance;
    }

}
