package org.userservice.user.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "users")
public class RegisteredUser extends BaseUser {
    @Field(name = "email")
    private String email;
    @Field(name = "firstname")
    private String firstname;
    @Field(name = "lastname")
    private String lastname;

    public RegisteredUser() {}

    public RegisteredUser(String username, String email,
                          String firstname, String lastname) {
        super(username);
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public RegisteredUser(String id, int version, String username,
                          String email, String firstname, String lastname) {
        super(id, version, username);
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
    }


    public String getEmail() {
        return email;
    }

    public RegisteredUser setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public RegisteredUser setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public RegisteredUser setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

}
