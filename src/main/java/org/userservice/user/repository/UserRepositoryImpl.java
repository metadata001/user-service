package org.userservice.user.repository;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.userservice.user.model.BaseUser;

@Repository
public class UserRepositoryImpl {
    private final MongoTemplate template;

    @Autowired
    public UserRepositoryImpl(MongoTemplate template) {
        this.template = template;
    }

    public <T extends BaseUser> T get(Query query,  Class<T> clazz) {
        return template.findOne(query, clazz);
    }

    public <T extends BaseUser> T add(T user) {
        return template.insert(user);
    }

    public <T extends BaseUser> UpdateResult update(Query query, Update update, Class<T> clazz) {
        return template.updateFirst(query, update, clazz);
    }

    public <T extends BaseUser> DeleteResult remove(Query query, Class<T> clazz) {
        return template.remove(query, clazz);
    }

}
