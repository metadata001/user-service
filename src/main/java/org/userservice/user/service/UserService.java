package org.userservice.user.service;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.userservice.user.dto.CreateUserRequest;
import org.userservice.user.dto.RemoveUserRequest;
import org.userservice.user.dto.UpdateUserRequest;
import org.userservice.user.mapper.UserMapper;
import org.userservice.user.model.RegisteredUser;
import org.userservice.user.repository.UserRepositoryImpl;

import static org.userservice.common.exceptions.handler.StaticExceptionHandlers.*;
import static org.userservice.user.mapper.RequestResultMappings.checkUserRequestWillUpdate;
import static org.userservice.user.service.query.UserQuery.getByUserNameQuery;
import static org.userservice.user.service.query.UserUpdate.getUserUpdate;

@Service
public class UserService {
    private final UserRepositoryImpl repository;
    private final UserMapper mapper;

    @Autowired
    public UserService(UserRepositoryImpl repository, UserMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public RegisteredUser getRegisteredUser(String username) {
        checkUserNameValid(username);
        var query = getByUserNameQuery(username);
        var result = repository.get(query, RegisteredUser.class);
        checkUserFound(username, result);
        return result;
    }

    public RegisteredUser createRegisteredUser(@Valid CreateUserRequest request) {
        var user = mapper.map(request);
        var query = getByUserNameQuery(request.getUsername());
        var check = repository.get(query, RegisteredUser.class);
        checkUserNameAvailable(check);
        return repository.add(user);
    }

    public UpdateResult updateRegisteredUser(@Valid UpdateUserRequest request) {
        var username = request.getUsername();
        checkUserNameValid(username);
        var query = getByUserNameQuery(username);
        var user = repository.get(query, RegisteredUser.class);
        checkUserRequestWillUpdate(user, request);
        return repository.update(query, getUserUpdate(request), RegisteredUser.class);
    }

    public DeleteResult removeRegisteredUser(@Valid RemoveUserRequest request) {
        var username = request.getUsername();
        checkUserNameValid(username);
        var query = getByUserNameQuery(request.getUsername());
        var result = repository.remove(query, RegisteredUser.class);
        checkUserRemoved(result, request);
        return result;
    }

}
