package org.userservice.user.service.query;

import org.springframework.data.mongodb.core.query.Query;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.userservice.common.helpers.ErrorValidationKeys.USERNAME;

public class UserQuery {

    public static Query getByUserNameQuery(String username) {
        return query(where(USERNAME).is(username));
    }

}
