package org.userservice.user.service.query;

import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.userservice.common.helpers.NonNullUpdate;
import org.userservice.user.dto.UpdateUserRequest;

import static org.userservice.common.helpers.UserCollectionKeys.*;



@Component
public class UserUpdate {

    public static Update getUserUpdate(UpdateUserRequest request) {
        var update = new NonNullUpdate();
        update.set(USERNAME, request.getUsername())
                .set(EMAIL, request.getEmail())
                .set(FIRSTNAME, request.getFirstname())
                .set(LASTNAME, request.getLastname())
                .inc(VERSION, 1);
        return update;
    }

}