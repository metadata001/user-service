package org.userservice.helper;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.userservice.common.helpers.ErrorValidationKeys;

class ErrorValidationKeysTest {

    @Test
    void getTargets_should_not_throw_error() {
        Assertions.assertDoesNotThrow(() -> {
            ErrorValidationKeys.getTargets();
        });
    }

}
