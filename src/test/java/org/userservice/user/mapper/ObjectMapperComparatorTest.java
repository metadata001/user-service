package org.userservice.user.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.userservice.user.dto.UpdateUserRequest;
import org.userservice.user.model.RegisteredUser;

import static org.userservice.user.mapper.ObjectMapperComparator.isUpdated;

class ObjectMapperComparatorTest {
    private static final String ID = "id";
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final int VERSION = 1;

    @Test
    void isUpdated_shouldReturn_trueIfUpdateValid() {
        var user = someRegisteredUser();
        var request = someUpdateUserRequest();
        var result = isUpdated(user, request);

        Assertions.assertThat(result).isTrue();
    }

    @Test
    void isUpdated_shouldReturnTrue_comparesNullFieldValues() {
        var user = someRegisteredUser();
        var request = someUpdateUserRequest()
                .setEmail(null);
        var result = isUpdated(user, request);

        Assertions.assertThat(result).isTrue();
    }

    @Test
    void isUpdated_shouldReturn_falseIfUpdateInValid() {
        var user = someRegisteredUser();
        var request = someUpdateUserRequest()
                .setEmail(EMAIL);
        var result = isUpdated(user, request);

        Assertions.assertThat(result).isFalse();
    }

    @Test
    void isUpdated_shouldReturn_falseIfParameterNull() {
        var user = someRegisteredUser();
        var result = isUpdated(user, null);

        Assertions.assertThat(result).isFalse();
    }

    private RegisteredUser someRegisteredUser() {
        var registeredUser = new RegisteredUser(ID, VERSION, USERNAME, EMAIL, FIRSTNAME, LASTNAME);
        registeredUser.setVersion(VERSION);
        return registeredUser;
    }

    private UpdateUserRequest someUpdateUserRequest() {
        String UPDATED = " updated";
        var request = new UpdateUserRequest();
        return request.setUsername(USERNAME)
                .setEmail(EMAIL + UPDATED)
                .setFirstname(FIRSTNAME)
                .setLastname(LASTNAME);
    }
}
