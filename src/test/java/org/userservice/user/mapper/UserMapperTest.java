package org.userservice.user.mapper;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.userservice.user.dto.CreateUserRequest;
import org.userservice.user.dto.UserDTO;
import org.userservice.user.model.RegisteredUser;

class UserMapperTest {
    private static UserMapper mapper;
    private static final String ID = "id";
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final int VERSION = 1;

    @BeforeAll
    public static void setup() {
        mapper = Mappers.getMapper(UserMapper.class);
    }

    @Test
    void mapperShould_mapRegisteredUser_toUserDto() {
        var registeredUser = someRegisteredUser();
        var userDTO = mapper.map(registeredUser);

        Assertions.assertThat(someUserDTO())
                .usingRecursiveComparison()
                .isEqualTo(userDTO);
    }

    @Test
    void mapperShould_mapUserDto_toRegisteredUser() {
        var userDTO = someUserDTO();
        var registeredUser = mapper.map(userDTO);

        Assertions.assertThat(someRegisteredUser())
                .usingRecursiveComparison()
                .isEqualTo(registeredUser);
    }

    @Test
    void mapperShould_mapCreateUserRequest_toRegisteredUser() {
        var createUserRequest = someCreateUserRequest();
        var registeredUser = mapper.map(createUserRequest);

        Assertions.assertThat(someRegisteredUser())
                .usingRecursiveComparison()
                .ignoringFields(ID)
                .isEqualTo(registeredUser);

    }

    private UserDTO someUserDTO() {
        return new UserDTO(ID, VERSION, USERNAME, EMAIL, FIRSTNAME, LASTNAME);
    }

    private RegisteredUser someRegisteredUser() {
        var registeredUser = new RegisteredUser(USERNAME, EMAIL, FIRSTNAME, LASTNAME);
        registeredUser.setId(ID);
        return registeredUser;

    }

    private CreateUserRequest someCreateUserRequest() {
        var createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername(USERNAME)
                .setEmail(EMAIL)
                .setFirstname(FIRSTNAME)
                .setLastname(LASTNAME);
        return createUserRequest;
    }

}
