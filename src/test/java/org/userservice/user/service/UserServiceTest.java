package org.userservice.user.service;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.assertj.core.api.Assertions;
import org.bson.BsonString;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.userservice.common.exceptions.InvalidUserNameException;
import org.userservice.common.exceptions.UserNameExistsException;
import org.userservice.common.exceptions.UserNotFoundException;
import org.userservice.common.exceptions.UserNotUpdatedException;
import org.userservice.user.dto.CreateUserRequest;
import org.userservice.user.dto.RemoveUserRequest;
import org.userservice.user.dto.UpdateUserRequest;
import org.userservice.user.dto.UserDTO;
import org.userservice.user.mapper.UserMapper;
import org.userservice.user.model.RegisteredUser;
import org.userservice.user.repository.UserRepositoryImpl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    private static final String ID = "id";
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";
    private static final String FIRSTNAME = "firstname";
    private static final String LASTNAME = "lastname";
    private static final int VERSION = 1;

    @Mock
    private UserRepositoryImpl repository;
    @Spy
    private UserMapper mapper = Mappers.getMapper(UserMapper.class);
    @InjectMocks
    private UserService service;

    @Test
    void getRegisteredUser_shouldReturn_RegisteredUser() {
        var registeredUser = someRegisteredUser();
        when(repository.get(any(), any())).thenReturn(registeredUser);
        var result = service.getRegisteredUser(USERNAME);

        Assertions.assertThat(result)
                .usingRecursiveComparison()
                .isEqualTo(someRegisteredUser());
    }

    @Test
    void getRegisteredUser_shouldThrow_invalidUserNameException() {
        var thrown = Assertions.catchThrowable(() -> service.getRegisteredUser(""));

        Assertions.assertThat(thrown).isInstanceOf(InvalidUserNameException.class);
    }

    @Test
    void getRegisteredUser_shouldThrow_userNotFoundException() {
        var thrown = Assertions.catchThrowable(() -> {
            when(repository.get(any(), any())).thenReturn(null);
            service.getRegisteredUser(USERNAME);
        });

        Assertions.assertThat(thrown).isInstanceOf(UserNotFoundException.class);
    }

    @Test
    void createRegisteredUser_shouldReturn_RegisteredUser() {
        var request = someCreateUserRequest();
        when(repository.add(any())).thenReturn(someRegisteredUser());
        var result = service.createRegisteredUser(request);

        Assertions.assertThat(result)
                .usingRecursiveComparison()
                .isEqualTo(someRegisteredUser());
    }

    @Test
    void createRegisteredUser_shouldThrow_UserNameExistsException() {
        var thrown = Assertions.catchThrowable(() -> {
            var request = someCreateUserRequest();
            when(repository.get(any(), any())).thenReturn(someRegisteredUser());
            service.createRegisteredUser(request);
        });

        Assertions.assertThat(thrown).isInstanceOf(UserNameExistsException.class);
    }

    @Test
    void updateRegisteredUser_shouldReturn_updateResult() {
        var user = someRegisteredUser();
        when(repository.get(any(), any())).thenReturn(user);
        when(repository.update(any(), any(), any())).thenReturn(someUpdateResult());
        var result = service.updateRegisteredUser(someUpdateUserRequest());

        Assertions.assertThat(result)
                .usingRecursiveComparison()
                .isEqualTo(someUpdateResult());
    }

    @Test
    void updateRegisteredUser_shouldThrow_invalidUserNameException() {
        var thrown = Assertions.catchThrowable(() -> {
            var request = someUpdateUserRequest();
            request.setUsername(null);
            service.updateRegisteredUser(request);
        });

        Assertions.assertThat(thrown).isInstanceOf(InvalidUserNameException.class);
    }

    @Test
    void updateRegisteredUser_shouldThrow_userNotUpdatedException() {
        var thrown = Assertions.catchThrowable(() -> {
            var request = someUpdateUserRequest()
                    .setEmail(EMAIL);
            when(repository.get(any(), any())).thenReturn(someRegisteredUser());
            service.updateRegisteredUser(request);
        });

        Assertions.assertThat(thrown).isInstanceOf(UserNotUpdatedException.class);
    }

    @Test
    void removeRegisteredUser_shouldReturn_DeleteResult() {
        var request = someRemoveUserRequest();
        when(repository.remove(any(), any())).thenReturn(someDeleteResult());
        var result = service.removeRegisteredUser(request);

        Assertions.assertThat(result)
                .usingRecursiveComparison()
                .isEqualTo(someDeleteResult());
    }

    @Test
    void removeRegisteredUser_shouldThrow_invalidUserNameException() {
        var thrown = Assertions.catchThrowable(() -> {
            var request = someRemoveUserRequest();
            request.setUsername(null);
            service.removeRegisteredUser(request);
        });

        Assertions.assertThat(thrown).isInstanceOf(InvalidUserNameException.class);
    }

    @Test
    void removeRegisteredUser_shouldThrow_userNotFoundException() {
        var thrown = Assertions.catchThrowable(() -> {
            var request = someRemoveUserRequest();
            var result = DeleteResult.acknowledged(0L);
            when(repository.remove(any(), any())).thenReturn(result);
            service.removeRegisteredUser(request);
        });

        Assertions.assertThat(thrown).isInstanceOf(UserNotFoundException.class);
    }

    private UserDTO someUserDTO() {
        return new UserDTO(ID, VERSION, USERNAME, EMAIL, FIRSTNAME, LASTNAME);
    }

    private RegisteredUser someRegisteredUser() {
        return new RegisteredUser(ID, VERSION, USERNAME, EMAIL, FIRSTNAME, LASTNAME);
    }

    private CreateUserRequest someCreateUserRequest() {
        var request = new CreateUserRequest();
        return request.setUsername(USERNAME)
                .setEmail(EMAIL)
                .setFirstname(FIRSTNAME)
                .setLastname(LASTNAME);
    }

    private RemoveUserRequest someRemoveUserRequest() {
        var request = new RemoveUserRequest();
        return request.setUsername(USERNAME);
    }

    private UpdateUserRequest someUpdateUserRequest() {
        String UPDATED = " updated";
        var request = new UpdateUserRequest();
        return request.setUsername(USERNAME)
                .setEmail(EMAIL + UPDATED)
                .setFirstname(FIRSTNAME)
                .setLastname(LASTNAME);
    }

    private UpdateResult someUpdateResult() {
        return UpdateResult.acknowledged(1L, 1L, new BsonString(ID));
    }

    private DeleteResult someDeleteResult() {
        return DeleteResult.acknowledged(1L);
    }

}
